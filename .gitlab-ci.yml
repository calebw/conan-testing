variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""

artifact:
  image: bash:latest
  script:
    - head -c 10m /dev/urandom | gzip -1 >10m.gz
  artifacts:
    paths:
      - 10m.gz
    expire_in: never

composer:
  image: dwdraju/alpine-curl-jq
  before_script:
    - apk update && apk add git
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - export NAMESPACE="${CI_PROJECT_ROOT_NAMESPACE}/composer-test-${VERSION}"
    - jq -r '.name = env.NAMESPACE' composer.json > composer2.json && rm composer.json && mv composer2.json composer.json
    - git remote remove origin
    - git remote add origin https://test:${PROJECT_ACCESS_TOKEN}@gitlab.com/${CI_PROJECT_PATH}
    - git config user.email test@example.com
    - git config user.name 'testuser'
    - git add .
    - git commit -m "update composer.json"
    - git tag -a $VERSION -m "composer release"
    - git push -o ci.skip --tags
  script:
    - cat composer.json
    - 'curl --header "Job-Token: $CI_JOB_TOKEN" --data tag=$VERSION "${CI_API_V4_URL}/projects/$CI_PROJECT_ID/packages/composer"'

conan:
  image: conanio/gcc7
  script:
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - conan remote add gitlab ${CI_API_V4_URL}/projects/$CI_PROJECT_ID/packages/conan
    - conan new conan/$VERSION -t
    - conan create . $CI_PROJECT_ROOT_NAMESPACE+$CI_PROJECT_NAME/stable
    - CONAN_LOGIN_USERNAME=ci_user CONAN_PASSWORD=${CI_JOB_TOKEN} conan upload conan/$VERSION@$CI_PROJECT_ROOT_NAMESPACE+$CI_PROJECT_NAME/stable --all --remote=gitlab

docker:
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  script:
    - cd docker/
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

generic:
  image: curlimages/curl:latest
  script:
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - tar -czvf test.tar.gz generic/
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file test.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/generic/$VERSION/test.tar.gz"'

maven:
  image: maven:3.6.3-jdk-11-slim
  script:
    - cd mvn/
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - export URL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/maven"
    - mvn deploy -s ci_settings.xml

npm:
  image: node:lts-alpine3.14
  before_script:
    - apk update && apk add jq
  script:
    - cd npm/
    - export NAMESPACE="@${CI_PROJECT_ROOT_NAMESPACE}/npm"
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - jq -r '.name = env.NAMESPACE | .version = env.VERSION' package.json > package2.json && rm package.json && mv package2.json package.json
    - cat package.json
    - |
      {
        echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
        echo "${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=\${CI_JOB_TOKEN}"
      } | tee -a .npmrc
    - cat .npmrc
    - npm publish

nuget:
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  script:
    - cd nuget
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - dotnet pack -c Release
    - dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
    - dotnet nuget push "bin/Release/*.nupkg" --source gitlab

pypi:
  image: python:3.7.10-alpine3.12
  before_script:
    - apk update && apk add python3-dev gcc libc-dev libffi-dev
    - pip install --upgrade pip
  script:
    - cd pypi/
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - pip install twine
    - python setup.py sdist bdist_wheel
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

.rubygem:
  image: ruby:latest
  before_script:
    - mkdir ~/.gem
    - echo "---" > ~/.gem/credentials
    - |
      echo "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/rubygems: '${CI_JOB_TOKEN}'" >> ~/.gem/credentials
    - chmod 0600 ~/.gem/credentials # rubygems requires 0600 permissions on the credentials file
  script:
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - cd rubygem/
    - gem build rubygem
    - gem push rubygem-$VERSION.gem --host ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/rubygems