# Maven Packages & the GitLab Package Registry

## The Important Bits

Our [documentation](https://docs.gitlab.com/ee/user/packages/maven_repository/#create-maven-packages-with-gitlab-cicd-by-using-maven) covers this topic quite well, so this will be breif. We care about the following bits:

- Name & Version
- Authentication

### Name & Version

You define the name and version of your Maven package within the project's `pom.xml` file. Here's how its defined for this project:

```xml
    <groupId>${env.CI_PROJECT_ROOT_NAMESPACE}</groupId>
    <artifactId>maven-test</artifactId>
    <packaging>jar</packaging>
    <version>${env.VERSION}</version>
```

We make use of a [predefined CI/CD variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) and an environment variable to specify the groupID and version here. The version is just randomly generated in the CI job (covered more below) and the groupID will equate to whatever the root namespace is for the project. 

Once built with the above configuration, the package name would be something like `calebw/maven-test` with a version of `1.23.45`.

If you attempt to publish a maven package with a name and version that already exists within the registry, the package files will simply be added alongside the package files that were already present. You will not receive an error like you would with other GitLab package types, such as NPM.

### Authentication

In order to publish a Maven package, there are a few places where we must configure our project to point to and authenticate with GitLab.

Firstly, we must ensure that we have defined a `<repositories>`, `<distributionManagement>` and (optionally) a `<snapshotRepository>` section in our project's `pom.xml` file. For this project, we define these values like so:

```xml
    <repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>${env.URL}</url>
    </repository>
    </repositories>
    <dependencies>
    </dependencies>
    <distributionManagement>
        <repository>
        <id>gitlab-maven</id>
        <url>${env.URL}</url>
        </repository>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>${env.URL}</url>
        </snapshotRepository>        
    </distributionManagement>
```

We define the repository with an ID of `gitlab-maven` and set the URL to the value of the environment variable `URL`. This variable is generated in the CI job which will be covered below.

With the repository ID and URL configured in the `pom.xml`, we then have to define a settings file to pass the `mvn` utility when attempting to publish our package. In this project, this is stored as `ci_settings.xml` as it's configured to use [predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) and will only work in the context of a pipeline.

The file looks like so:

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Job-Token</name>
            <value>${env.CI_JOB_TOKEN}</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```

Here we reference the id `gitlab-maven` that we configured in the `pom.xml` earlier. With this, we specifiy that this repository ID should authenticate against the URL defined in the `pom.xml` with the `CI_JOB_TOKEN` value.

In summary, we define our repository ID and URL in the `pom.xml` file, and using the `ci_settings.xml` file (which is passed in at the command line) we tell maven what values this repository ID should authenticate with against the defined URL.

----

## CI Job Overview

Here's the CI job for publishing the Maven package:

```yaml
maven:
  image: maven:3.6.3-jdk-11-slim
  script:
    - cd mvn/
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - export URL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/maven"
    - mvn deploy -s ci_settings.xml
```

After moving into the `mvn/` directory where our Maven project is stored, we export the `VERSION` variable for the package:

```sh
export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
# This would equate to something like VERSION=1.23.45
```

This is simply generating a random version value and assigning it to the `VERSION` environment variable, which is called directly within the `pom.xml` to specify the version of the package. This grabs a random number between 0-9, and a random number between 0-99 for the last two decimals.

We then export the `URL` variable:

```sh
export URL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/maven"
```

This makes use of some pre-defined CI/CD variables to automatically fetch the appropriate API endpoint for the project. This is used in the `pom.xml` to specify the URL for our `gitlab-maven` repository ID, which tells Maven where we want to publish the package.

After this, we then build and publish the package via the `mvn` utility:

```sh
mvn deploy -s ci_settings.xml
```

Here we specify the `-s` flag, which allows us to directly reference a settings file. We pass the `ci_settings.xml` file in here to tell maven what to use to authenticate with the repository defined in our `pom.xml`. 