# artifact-packages

Testing project to automatically generate a CI artifact, a package of each type and a Docker image via a pipeline.

## Getting started

Create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token) with the scope `write_repository`. Store this secret in a CI/CD variable named `PROJECT_ACCESS_TOKEN`. This is required to automate the publishing of [Composer packages](https://docs.gitlab.com/ee/user/packages/composer_repository/).

The pipeline makes use of [predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to automatically adjust where a package should be pushed, and it changes the package versions randomly. After creating the `PROJECT_ACCESS_TOKEN` variable, simply start a pipeline!

## Package primer & CI breakdown

A summary of how each package type works, along with a detailed description of what each package's relative CI job is doing can be found in the `/docs` directory for each package. For example, you can find these details for the NPM package in the [`docs/npm.md` file](/docs/npm.md).

## Caveats

Ruby Gems aren't enabled on GitLab.com yet, so the job responsible for ruby gems has been "disabled" in the CI/CD script.
